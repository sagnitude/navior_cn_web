﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<!-- 注意，所有需要使用文本的地方，使用C#脚本切换语言 -->
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>耐威科技</title>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        <link href="style/body.css" rel="stylesheet">
        <link href="style/jquery-ui.css" rel="stylesheet">
        <!--<script type="text/javascript" src="js/jquery.easing.compatibility.js"></script>-->
        <script type="text/javascript" src="js/languageselector.js"></script>
    </head>
    <body>
        <form id="form1" runat="server">
        <div>
            
        </div>
        </form>
        <!-- Navigator -->
        <div class="navigator">
	        <div class="navi_body">
		        <div class="logo">
			        <a href="index.aspx"><img alt="navior" src="images/logo.png" /> </a></div>
		        <div class="bar">
                    
			        <ul id="menu_topbar" class="menu_topbar">
				        <li><a href="">About</a></li>
				        <li id="menu_1"><a href="javascript:void(0);" onclick="showHiddenPanel(2);">Products</a></li>
				        <li><a href="">News</a></li>
				        <li><a href="">Online</a></li>
				        <li><a href="">Retail</a></li>
				        <li><a href="">ContactUs</a></li>
			        </ul>
		        </div>
		        <div class="language_selector">
                    <ul id="menu">
                        <li id="current_language"><a href="javascript:void(0);" onclick="toggleLanguages()">CurrentLanguage</a></li>
                        <li class="hidden_language"><a href="#">Language1</a></li>
                        <li class="hidden_language"><a href="#">Language2</a></li>
                    </ul>
		        </div>
	        </div>
        </div>
        <!-- Hidden Area : Popup Menu -->
        <div id="hidden_menu2" class="hidden">
        </div>
        <!-- Rolling Area -->
        <div class="main">
	        <div class="rolling_area">
	            <div class="body_wrapper">
	                <div id="rolling_container">
	
	                </div>
                    <div class="rolling_navigator">
                        <ul>
                            <!-- Many Dots Here! We can consider to make the amount automatically adds while a new picture is added in -->
                            <li class="navi_dot"></li>
                        </ul>
                    </div>
	            </div>
	        </div>
        </div>
        <!-- Gadgets Here -->
        <div class="gadgets">
	        <!-- 这里是小模块 -->
	        <div class="news gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>	
	        <div class="products gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>
	        <div class="taobao gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>
	        <div class="services gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>
	        <div class="weibo gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>
	        <div class="app gadget">
                <div class="gadget_name">
                </div>
                <div class="gadget_container">
                </div>
            </div>
        </div>
        <div class="declaration">
	        <!-- 这里有个hr -->
	        <div class="line"></div>
	        <div class="upper_dec">
                <ul class="bottom_navi">
                <li class="bottom_navi_element"><a href="">Join Us</a></li>
                <li class="bottom_navi_element"><a href="">Cooperation</a></li>
                <li class="bottom_navi_element"><a href="">Legal Notice</a></li>
                <li class="bottom_navi_element"><a href="">Weibo</a></li>
                </ul>
	        </div>
	        <div class="lower_dec">
                <ul>
                <li class="declare">400</li>
                <li class="declare">Company</li>
                <li class="declare">Website</li>
                </ul>
	        </div>
        </div>
        
        <div style="display:none">
            <div id="popup_menu2"></div>
            <div id="popup_menu3"></div>
            <div id="popup_menu5"></div>
        </div>
    </body>



</html>
