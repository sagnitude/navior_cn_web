﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en">
    <!-- MEMO:字体：微软雅黑 14字号，字体调小 -->
    <!-- MEMO:隔开导航栏 -->
    <!-- MEMO:png在IE FF Safari中的自动添加的阴影的问题 -->
    <!-- 语言解决方案：对index，使用session变量，然后调用js，读取语言文件，动态改变
        其他的文档，通过session变量判断，从虚拟路径调用到具体的语言路径，或者可以使用重定向
     -->
    <!-- MEMO: 用ajax或者普通js，载入navigator.html等碎片，而不是复制。 -->


    <!-- MEMO: 字体，语言支持 -->
    <!-- MEMO: <a>的onmouseover变色 -->
    <!-- MEMO: CSS3的一些特效 -->
    <!-- 语言支持想法1：需要的地方都用span，使用id，使用js载入时读取并替换；或者用很多span，只显示current language id -->
    <!-- MEMO: 把js的效果改成jquery的动画 -->
    <head>
        <meta charset="utf-8" />
        <title>Navior</title>
        <script type="text/javascript" src="/js/jquery-ui.js"></script>
        <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
        <link href="/parts/navigator.css" rel="stylesheet">
        <link href="/parts/body.css" rel="stylesheet">
        <link href="/style/body2.css" rel="stylesheet">
        <link href="/style/utils.css" rel="stylesheet">
        <link href="/parts/footer.css" rel="stylesheet">
        <link href="/style/jquery-ui.css" rel="stylesheet">
        <link href="/lib/nivo-slider/nivo-slider.css" rel="stylesheet" >
        <link href="/lib/nivo-slider/themes/default/default.css" rel="stylesheet">
        <link rel="stylesheet" href="/en/en.css">
        <script type="text/javascript" src="/parts/footer.js"></script>
        <script type="text/javascript" src="/parts/navigator.js"></script>
        <script type="text/javascript" src="/js/languageselector.js"></script>
        <script type="text/javascript" src="/js/index2body.js"></script>
        <script type="text/javascript" src="/lib/nivo-slider/jquery.nivo.slider.2.js"></script>
        <link href="/en/index2_en.css" rel="stylesheet">
        <script type="text/javascript">
            $(window).load(function () {
                 $('.returntolist').attr("href","/en/news.html");
               $('#slider').nivoSlider({
                    effect: 'boxRain',               // Specify sets like: 'fold,fade,sliceDown'
                    slices: 15,                     // For slice animations
                    boxCols: 8,                     // For box animations
                    boxRows: 4,                     // For box animations
                    animSpeed: 500,                 // Slide transition speed
                    pauseTime: 3500,                // How long each slide will show
                    startSlide: 0,                  // Set starting Slide (0 index)
                    directionNav: false,             // Next & Prev navigation
                    directionNavHide: true,
                    controlNav: true,               // 1,2,3... navigation
                    controlNavThumbs: false,        // Use thumbnails for Control Nav
                    pauseOnHover: true,             // Stop animation while hovering
                    manualAdvance: false,           // Force manual transitions
                    prevText: 'Prev',               // Prev directionNav text
                    nextText: 'Next',               // Next directionNav text
                    randomStart: false,             // Start on a random slide
                    beforeChange: function () { },     // Triggers before a slide transition
                    afterChange: function () { },      // Triggers after a slide transition
                    slideshowEnd: function () { },     // Triggers after all slides have been shown
                    lastSlide: function () { },        // Triggers when last slide is shown
                    afterLoad: function () { }         // Triggers when slider has loaded
                });
            });
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
        <div>
            
        </div>
        </form>
        <!-- ENGLISH NAVIGATOR SRC -->
        <div class="navigator">
            <div class="navigator_wrapper wrapper">
                <div class="logo">
                    <a href="/en/index2.html"></a>
                </div>
                <div class="language_switch">
                    <a href="http://www.navior.cn/index2.html" id="cn">中文版&nbsp&nbsp</a>
                    <span>|</span>
                    <a href="http://www.navior.cn/en/index2.html" id="eng">&nbsp&nbspEnglish</a>
                </div>
                <div class="bar">
                    <div class="bar_container">
                        <a id="0" href="/en/history.html"><span id="about_navi">About</span></a><em>|</em>
                        <a id="1" href="/en/products.html"><span id="products">Product</span></a><em>|</em>
                        <a id="2" href="/en/news.html"><span id="news">News</span></a><em>|</em>
                        <a id="3" href="http://www.aliexpress.com/store/627963" target="_blank"><span id="online">BUY NOW</span></a><em>|</em>
                        <a id="4" href="http://www.aliexpress.com/store/627963" target="_blank"><span id="retail">Retail</span></a><em>|</em>
                        <a id="5" href="/en/contactus.html"><span id="contact_us">Contact</span></a>
                    </div>
                    <!--<span id="floater" style="display: none"></span>-->
                </div>

            </div>
        </div>




        <div class="clear"></div>

        <!-- 浮动的菜单栏，和滚动区域重合，使用zIndex使之默认被覆盖 -->
        <!-- 菜单栏取消，整合到分页面中
        <div class="menues">
            <div class="hidden_backgrounds">
                <div class="hidden_background" id="hidden_background1">
                </div>
                <div class="hidden_background" id="hidden_background2">
                </div>
                <div class="hidden_background" id="hidden_background3">
                </div>
                <div class="hidden_background" id="hidden_background4">
                </div>
                <div class="hidden_background" id="hidden_background5">
                </div>
                <div class="hidden_background" id="hidden_background6">
                </div>
            </div>
            <div class="menu_wrapper wrapper">
                <div class="hidden_menu" id="hidden_menu1">
                    <div class="hidden_content">
                    </div>
                </div>
                <div class="hidden_menu" id="hidden_menu2">
                    <div class="hidden_content">
                    </div>
                </div>
                <div class="hidden_menu" id="hidden_menu3">
                    <div class="hidden_content">
                    </div>
                </div>
                <div class="hidden_menu" id="hidden_menu4">
                    <div class="hidden_content">
                    </div>
               </div>
                <div class="hidden_menu" id="hidden_menu5">
                    <div class="hidden_content">
                    </div>
                </div>
                <div class="hidden_menu" id="hidden_menu6">
                    <div class="hidden_content">
                    </div>
                </div>
            </div>
        </div>
        -->


        <!-- 滚动区域，和导航条相接,background到此为止 -->
        <!-- 需求资源文件：wrapper需要透明背景的图片，main需要每张图片的背景 -->
       <div class="main">
            <div class="main_wrapper wrapper slider-wrapper theme-default">
                <div class="ribbon"></div>
                <div class="rolling_area nivoSlider" id="slider">
                    <img src="/images3/index_rolling_en_0.jpg" alt="SLIDER1"/>
                    <img src="/images3/index_rolling_en_1.jpg" alt="SLIDER2"/>
                    <img src="/images3/index_rolling_en_2.jpg" alt="SLIDER3"/>
                </div>
            </div>
        </div>

        <!-- Gadgets -->
        <div class="gadgets">
            <div class="gadgets_wrapper wrapper">
                <div class="clear"></div>
                <div class="gadget" id="news_gadget">
                    <div class="gadget_title_line">
                        <span>News</span><a href="/en/news.html">More></a>
                    </div>
                    <div class="gadget_content">
                        <ul>
                            <li><em></em><a href="/en/news.html#news_1">Welcome Your Visiting Navior at HK Electronics Fair</a></li>
                            <li><em></em><a href="/en/news.html#news_2">Unexpected Achievement from Pet Fair Asia 2013, August 22-25</a></li>
                            <li><em></em><a href="/en/news.html#news_3">Navior Got Approval in CBME </a></li>
                            <li><em></em><a href="/en/news.html#news_4">Navior iC Shown Up on the Most Influential Weiphone Website</a></li>
                        </ul>
                    </div>
                </div>
                <div class="gadget" id="products_gadget">
                    <div class="gadget_title_line">
                        <span>New Arrival</span>
                    </div>
                    <div class="gadget_content">
                        <a href="/en/ipower.html"> </a>
                    </div>
                </div>
                <div class="gadget" id="app_gadget">
                    <div class="gadget_title_line">
                        <span>APP</span>
                    </div>
                    <div class="gadget_content">
                        <a id="android" href="javascript:void(0);"></a>
                        <a id="ios" href="https://itunes.apple.com/cn/app/navior-ic/id580769446?mt=8"></a>
                    </div>
                </div>
                <div class="gadget" id="taobao_gadget">
                    <div class="gadget_title_line">
                        <span>Aliexpress online store</span>
                    </div>
                    <div class="gadget_content">
                        <a href="http://www.aliexpress.com/store/627963"></a>
                    </div>
                </div>
                <div class="gadget" id="weibo_gadget">
                    <div class="gadget_title_line">
                        <span>Facebook</span>
                    </div>
                    <div class="gadget_content">
                        <a href="https://www.facebook.com/pages/Navior-Technology-Co-Ltd/286569531481830"> </a>
                    </div>
                </div>
                <div class="gadget" id="services_gadget">
                    <div class="gadget_title_line">
                        <span>gCookie Platform</span>
                   </div>
                    <div class="gadget_content">
                        <a href="http://navior.cn/cs/"> </a>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>


        <!-- THE ORIGINAL ENGLISH FOOTER -->
        <!-- declarations -->
        <div class="declaration">
            <div class="declaration_wrapper wrapper">
                <div class="declaration_links">
                    <ul>
                        <li><a href="/en/joinus.html">Partner with us   &nbsp</a></li>
                        <li><a href="/en/law.html">Legal Notice</a></li>
                        <li><a href="#">Facebook</a></li>
                        <li id="languages"><a id="zhcn" href="#"></a><a id="en" href="#"></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="declaration_info">
                    <span>Domestic Hotline：+86 400-110-0877&nbsp&nbsp&nbsp&nbspNavior Technology CO., Ltd&nbsp&nbsp苏ICP12026719号</span>
                </div>
            </div>
        </div>


    
    </body>
</html>
