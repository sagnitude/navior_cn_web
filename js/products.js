$(document).ready(function () {
    /*
    $('#ic-lite').hover(function () {
    $('#ic-lite').animate({
    opacity:'0'
    },"100");


    }, function () {
    $('#ic-lite').css("background-image", "url('/images3/l2-2.png')");
    });
    $('#ic-pro').hover(function () {
    $('#ic-pro').css("background-image", "url('/images3/l1-2.png')");
    }, function () {
    $('#ic-pro').css("background-image", "url('/images3/l1-1.png')");
    });
    $('#ic-classic').hover(function () {
    $('#ic-classic').css("background-image", "url('/images3/l3-1.png')");
    }, function () {
    $('#ic-classic').css("background-image", "url('/images3/l3-2.png')");
    });
    */

    $('.hover').css('opacity', 0);

    $('#ic-lite').hover(function () {
        $('#ic-lite .hover').stop().animate({opacity: '1'},80);
    }, function () {
        $('#ic-lite .hover').stop().animate({opacity: '0'},80);
    });

    $('#ic-pro').hover(function () {
        $('#ic-pro .hover').stop().animate({opacity: '1'},80);
    }, function () {
        $('#ic-pro .hover').stop().animate({opacity: '0'},80);
    });

    $('#ic-classic').hover(function () {
        $('#ic-classic .hover').stop().animate({opacity: '1'},80);
    }, function () {
        $('#ic-classic .hover').stop().animate({opacity: '0'},80);
    });
});