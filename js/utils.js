$(document).ready(function () {
    $('body').bind("selectstart", function () { return false; });
    $('.current_product').css("border-bottom", "2px solid #2EA7E0");
    $('.current_menu').css("color","#2FA8E1");
});


function toggleHiddenProducts(a){
    if(a==1){
        $('.more ul').css("display", "block");
    }else{
        $('.more ul').css("display", "none");
    }
}