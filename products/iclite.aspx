﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>耐威科技</title>
        <script type="text/javascript" src="/js/jquery-ui.js"></script>
        <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/js/utils.js"></script>
        <link href="/style/utils.css" rel="stylesheet">
        <link href="/style/product.css" rel="stylesheet">
        <link href="/style/jquery-ui.css" rel="stylesheet">
        <!--<script type="text/javascript" src="/js/jquery.easing.compatibility.js"></script>-->
    </head>
    <body>
        <!-- 导航条，最上面一条 -->
        <div class="navigator">
            <div class="navigator_wrapper wrapper">
                <div class="logo">
                    <a href="index2.html"><img alt="navior" src="/images2/logo.png" /> </a>
                </div>
                <div class="bar">
                    <div class="bar_container">
                        <a href="#"><span id="about_navi">关于耐威</span></a>
                        <a href="#" class="current_menu"><span id="products">产品中心</span></a>
                        <a href="#"><span id="news">新闻动态</span></a>
                        <a href="#"><span id="online">在线销售</span></a>
                        <a href="#"><span id="retail">零售店</span></a>
                        <a href="#"><span id="contact_us">联系我们</span></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="submenu">
            <ul class="smenu">
                <li class="current_product"><a href="">iC-Lite</a></li>
                <li><a href="">iC-Pro</a></li>
                <li><a href="">iC-Classic</a></li>
                <li><a href="">iTank智能对战坦克</a></li>
                <li><a href="">iPower智能移动电源</a></li>
                <li class="more" onmouseover="javascript:toggleHiddenProducts(1);"><a href="">更多产品</a>
                    <ul onmouseout="javascript:toggleHiddenProducts(2);">
                        <li><a href="">nLocator智能防丢器</a></li>
                        <li><a href="">iCar智能无线赛车</a></li>
                        <li><a href="">小熊星</a></li>
                    </ul>
                </li>
            </ul>
        
        </div>
        <div class="introduction">
             <div class="intro_left intro" id="intro_1">
                 <div class="text">
                     <h1>iC-Lite</h1>
                     <p>年轻，就要多彩</p>
                 </div>
             </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_right intro" id="intro_2">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
                 
             </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_left intro" id="intro_3">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
            </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_right intro" id="intro_4">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
            </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_left intro" id="intro_5">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
             </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_right intro" id="intro_6">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
             </div>
             <div class="clear">
                 <!-- 阴影条，放在两个introduction块中间，z-index较低，被覆盖。 -->
                 <img src="/images2/shadow.png" alt="shadow"/></div>
             <div class="intro_left intro" id="intro_7">
                 <div class="text">
                 <h1>极致绚丽的色彩王国</h1>
                 <p>耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的耐威iC-Lite活泼大胆的</p>
                 </div>
             </div>
             <div class="clear"></div>
        
        </div>


        <!-- declarations -->
        <div class="declaration">
            <div class="declaration_wrapper wrapper">
                <div class="declaration_links">
                    <ul>
                        <li><a href="#">工作机会</a></li>
                        <li><a href="#">商户加盟</a></li>
                        <li><a href="#">法律声明</a></li>
                        <li><a href="#">官方微博</a></li>
                        <li id="languages"><a id="zhcn" href="#"></a><a id="en" href="#"></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="declaration_info">
                    <span>全国客服热线：400-110-0877&nbsp&nbsp&nbsp&nbsp江苏耐威科技有限公司&nbsp&nbsp苏ICP12026719号</span>
                </div>
            </div>
        </div>
    </body>
</html>
